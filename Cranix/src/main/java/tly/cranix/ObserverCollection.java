package tly.cranix;

import java.util.Iterator;
import java.util.Vector;

import tly.cranix.type.IObserver;

public class ObserverCollection implements Iterable, IObserver{
	private Vector<IObserver> obsCollection;
	
	private class ObsIterator implements Iterator{
		private int current;

		public ObsIterator(){
			current = -1;
		}
		
		public boolean hasNext() {			
			if(obsCollection.size() <= 0) return false;
			if(current == obsCollection.size()-1) return false;
			return true;
		}

		public IObserver next() {
			current++;
			return obsCollection.elementAt(current);
		}

		public void remove() {
			obsCollection.remove(current);
			--current;
		}
	}
	
	// Constructor
	ObserverCollection(){
		obsCollection = new Vector<IObserver>();
	}
	
	public void update(GameWorldProxy gwp){		
		Iterator itr = this.iterator();
		while(itr.hasNext())
			((IObserver)itr.next()).update(gwp);
	}
	
	public IObserver at(int index){
		return obsCollection.elementAt(index);
	}
	
	public void add(IObserver g) {
		obsCollection.add(g);
	}
	
	public boolean isEmpty() {
		return obsCollection.isEmpty();
	}

	public Iterator iterator() {
		return new ObsIterator();
	}

	public int size() {
		return obsCollection.size();
	}
}