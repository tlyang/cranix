package tly.cranix;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import tly.cranix.model.Car;
import tly.cranix.model.FieldSquare;
import tly.cranix.model.GameObject;
import tly.cranix.model.Monsterball;
import tly.cranix.model.SmartBomb;
import tly.cranix.model.Sound;
import tly.cranix.model.TimeTicket;
import tly.cranix.strategy.DefaultStrategy;
import tly.cranix.strategy.ISmartBombStrategy;
import tly.cranix.type.ICollider;
import tly.cranix.type.IGameWorld;
import tly.cranix.type.IObservable;
import tly.cranix.type.IObserver;
import tly.cranix.type.ISelectable;
import tly.cranix.type.Moveable;

public class GameWorld extends JPanel implements IGameWorld, IObservable{
	
/** Final Constants ***********************************************************/
	private static final long serialVersionUID = 1L;
	private static final int CAR_HEIGHT = 12;
	private static final int CAR_WIDTH = 7;
	private static final int DEFAULT_CAR_SPEED = 4;
	private static final int INIT_CLOCK_TIME = 100;
	private static final int INIT_LEVEL = 1;
	private static final double INIT_MIN_SCORE_PERCENT = .5;
	private static final int INIT_TICKET_TIME = 5;
	private static final double SCORE_INC_PER_LEVEL = .1;
	private static final int MAX_LIVES = 3;
	private static final int MAX_SPEED = 6;
	private static final int MAX_SCORE = 100;
	private static final int MB_RADIUS = 6;
	private static final int NO_LIVES = 0;
	private static final int SB_SIZE = 10;
	private static final int THE_CAR = 0;
	private static final int TICKET_WIDTH = 15;
	private static final int TICKET_HEIGHT = 15;
	private static final int TICKET_DECREMENT = 1;
	private static final int TIME_DECREMENT = 2;
	private static final int TIME_UP = -1;
	
	public static final int CAR_SPEED_INCREMENT = 1;
	public static final Color CHASE = new Color((int)0xFF, (int)0x19, (int)0x19);
	public static final int COLOR_MAX_BIT = 256;
	public static final int COLLIDE_DELAY = 90; // milliseconds
	public static final int FIELD_LENGTH = 100;
	public static final int FIELD_SHIFT = 0;
	public static final int FIELD_UNIT_SIZE = 6;
	public static final int FIELD_SIZE = FIELD_LENGTH * FIELD_UNIT_SIZE;
	public static final int MAX_HEADING_DEG = 360;
	public static final double MAX_SQUARES = FIELD_LENGTH * FIELD_LENGTH;
	public static final Color MAYBE_OWNED = Color.BLUE;
	public static final Color OWNED = new Color((int)0x7B, (int)0xCB, (int)0x61 );
	public static final Color SELECTED_COLOR = new Color((int)0xAC, (int)0xFF, (int)0x75);
	public static final double START_X = FIELD_SIZE/2+FIELD_SHIFT;
	public static final double START_Y = FIELD_SHIFT;
	public static final double WIN_SIZE = FIELD_SIZE + FIELD_UNIT_SIZE + FIELD_SHIFT;

	public static final int DECREMENT = -1;
	public static final int INCREMENT = 1;
	// Loaded in Eclipse, the sound directory is:
	public static final String SOUND_DIR =  "src" + 
												 File.separator + "main" + 
												 File.separator + "resources" + 
												 File.separator + "tly" + 
												 File.separator + "cranix" + 
												 File.separator + "audio" + 
												 File.separator;
	
/** Attributes ****************************************************************/
	private int livesLeft;
	private int gameClock;
	private int currentLevel;
	private double minScore;	// a percentage / Target score
	private int ownedSquares;
	private boolean sound; 
	private boolean clockState;
	private ISmartBombStrategy currentStrategy;
	private GameObjectCollection gObjects;
	private GameObjectCollection fsObjects;
	private ObserverCollection goObservers;
	
	private String collisionSoundFile = "ticket_up.wav";
	private Sound ticketSound;
	private String mbCollisionSoundFile = "mb_zap.wav";
	private Sound mbSound;
	private String sbCollisionSoundFile = "sb_boom.wav";
	private Sound sbSound;
	private String track = "debol_hypnodoum2.wav";
	private Audio trackSound;
	
	private double winLeft, winBot, winTop, winRight, winWidth, winHeight;

	public double getWinLeft(){ return winLeft; }
	public double getWinBot(){ return winBot; }
	public double getWinRight(){ return winRight; }
	public double getWinTop(){ return winTop; }
	
	public void setWinTop(double newT){ 
		winTop = newT; 
		updateHeight();
	}
	
	public void setWinRight(double newR){ 
		winRight = newR; 
		updateWidth();
	}
	
	public void setWinBot(double newB){ 
		winBot = newB; 
		updateHeight();
	}
	
	public void setWinLeft(double newL){ 
		winLeft = newL; 
		updateWidth();
	}

	public double windowWidth(){ return winWidth; }
	public double windowHeight(){ return winHeight; }
	
	private void updateHeight(){
		winHeight = winTop - winBot;
	}
	
	private void updateWidth(){
		winWidth = winRight - winLeft;
	}
	
	public static void print(String s){
		System.out.println(s);
	}
	
////////////////////////////////////////////////////////////////////////////////
	
/** Constructor ***************************************************************/
	GameWorld(){
		goObservers = new ObserverCollection();
		gObjects = new GameObjectCollection();
		fsObjects = new GameObjectCollection(FIELD_LENGTH*FIELD_LENGTH);

		sound = true;
		currentLevel = 1;
		ownedSquares = 0;
		gameClock = initClock();
		livesLeft = MAX_LIVES;
		minScore = calcMinScore();
		currentStrategy = new DefaultStrategy();
		clockState = false;
		
		/* Start bottom left at (0,0) */
		winLeft = winBot = 0;
		winTop = winRight = WIN_SIZE;
		winWidth = winRight - winLeft;
		winHeight = winTop - winBot;

		mbSound = new Sound(GameWorld.SOUND_DIR + mbCollisionSoundFile);
		ticketSound = new Sound(GameWorld.SOUND_DIR + collisionSoundFile);
		sbSound = new Sound(GameWorld.SOUND_DIR + sbCollisionSoundFile);
		trackSound = new Audio(GameWorld.SOUND_DIR+track);
		//trackSound.setPriority(10);
		trackSound.start();

		newCar();
		initField();
	}
	
/** static methods ************************************************************/
	
	static public Color randColor(){
		return new Color(randomInt(COLOR_MAX_BIT), randomInt(COLOR_MAX_BIT), 
					randomInt(COLOR_MAX_BIT));
	}

	static public int getInitTicketTime(){
		return INIT_TICKET_TIME;
	}
	
	static public int randomInt(int upperBound){
		Random randNum = new Random();
		return randNum.nextInt(upperBound);
	}
			
	static public float randomFloat(int upperBound){
		Random randNum = new Random();
		return randNum.nextFloat() * upperBound;
	}
			
	static public int randomInt(int lowerBound, int upperBound){
		Random randNum = new Random();
		return randNum.nextInt(upperBound) + lowerBound;
	}
	
/** Private methods ***********************************************************/
	
	private int calcTicketTimer(int currentLevel){
		return INIT_TICKET_TIME - (TICKET_DECREMENT * (currentLevel - 1));
	}
	
	private float[] checkLocation(float x, float y, int height, int width){
		float [] xy = {x, y};

		float x0 = (float)(1.5*FIELD_UNIT_SIZE) + FIELD_SHIFT;
		float xN = FIELD_SIZE+FIELD_SHIFT-FIELD_UNIT_SIZE/2;
		float y0 = FIELD_SHIFT+FIELD_UNIT_SIZE/2;
		float yN = FIELD_SIZE-(float)(1.5*FIELD_UNIT_SIZE)+FIELD_SHIFT;
		
		if(x < x0) xy[0] = x0;
		else if(x+width > xN) xy[0] = xN-width;
		
		if(y < y0) xy[1] = y0;
		else if(y+height > yN) xy[1] = yN-height;
		
		return xy;
	}
	
	private void newCar(){
		addObject(new Car(START_X, START_Y, Color.ORANGE, CAR_WIDTH, CAR_HEIGHT, 
				Moveable.SOUTH, DEFAULT_CAR_SPEED));
	}
	
	private int initClock(){
		return INIT_CLOCK_TIME-((currentLevel-1)*TIME_DECREMENT);
	}
	
	private void reInitField(){
		fsObjects.clear();
		initField();
	}
	
	private void setTicketTimes(){
		Iterator<GameObject> goItr = gObjects.iterator();
		GameObject go;
		while(goItr.hasNext()){
			go = (GameObject)goItr.next();
			if(go instanceof TimeTicket){
				((TimeTicket)go).setTime(((TimeTicket)go).getTime()-(getLevel()-1));
			}
		}
	}

/** Audio methods *************************************************************/
	public void playTrack(){
		trackSound.play();
	}
	
	public void pauseTrack(){
		trackSound.pause();
	}
	
	public void endTrack(){
		trackSound.end();
	}
	
	public void toggleTrack(){
		if(trackSound.isActive()) pauseTrack();
		else playTrack();
	}

/** IObservable methods *******************************************************/

	public void addObserver(IObserver obs){
		goObservers.add(obs);
	}

	public void notifyObservers(){
		goObservers.update(new GameWorldProxy(this));
	}
	
/** IGameWorld/Accessors ******************************************************/
	
	public Car getCar(){ return (Car)gObjects.at(THE_CAR); }
	
	public GameObjectCollection getGameCollection(){ return gObjects; }
	
	public GameObjectCollection getFieldCollection(){ return fsObjects; }
		
	public int getLevelTicketTime(){
		int time = INIT_TICKET_TIME - (getLevel() - 1);
		return (getLevel() >= 5) ? 1 : time;
	}
	
	public ObserverCollection getObserverCollection(){ return goObservers; }

	public int getOwnedSqrs(){ return ownedSquares; }
	
	public double getScore(){ 
		return 100 * (ownedSquares/MAX_SQUARES); 
	}
	
	public int getLevel(){ return currentLevel; }
	
	public int getLivesLeft(){ return livesLeft; }
	
	public int getClock(){ return gameClock; }

	public boolean getClockState(){ return clockState; }

	
	private double calcMinScore(){
		double minimum = 100 * (INIT_MIN_SCORE_PERCENT + 
					((currentLevel-1)*SCORE_INC_PER_LEVEL));
		return (minimum < MAX_SCORE) ? minimum : MAX_SCORE;
	}
	
	public double getMinScore(){
		return minScore;
	}
	
	public int getNonOwnedSqrs(){
		return (int)MAX_SQUARES - ownedSquares;
	}
	
	public boolean getSoundState(){ return sound; }
	
	public boolean reachedMinScore(){
		return (getScore() >= getMinScore()) ? true : false;
	}
	
	
/** GameWorld mutators ********************************************************/
	
	public void addOwnedSquares(int newSquares){
		ownedSquares += newSquares;
		notifyObservers();
	}
	
	public void addObject(GameObject go){
		gObjects.add(go);
		notifyObservers();
	}
	
	public void changeSpeed(GameObject go, int incDir){
		// incDir is either +1 or -1
		if(go instanceof Moveable){
			int currSpeed = getCar().getSpeed();
			if(currSpeed > 0){
				getCar().setSpeed(currSpeed+incDir);
			}else{
				if(incDir > 0) 
					getCar().setSpeed(currSpeed+incDir);
			}
		}
		notifyObservers();
	}
	
	public void checkCollisions(){
		ArrayList <ICollider> alreadyCollided = new ArrayList<ICollider>();
		Iterator<GameObject> goItr = gObjects.iterator();
		ICollider obj1;
		ICollider obj2;
		
		obj1 = (ICollider)goItr.next();
		
		while (goItr.hasNext()){
			obj2 = (ICollider)goItr.next();
			
			if(obj1 != obj2 && obj1.collidesWith(obj2) && !alreadyCollided.contains(obj2) ){
				if(obj2 instanceof Monsterball){
					
					/* Monsterball collision */
					obj2.handleCollision(obj1);
					if(sound)
						mbSound.play();
					resetCarLocation();
					incLivesLeft(GameWorld.DECREMENT);
					if(getLivesLeft() <= 0) gameOver(TIME_UP);
					goItr.remove();
				}else if(obj2 instanceof TimeTicket){
					
					/* Time ticket collision */
					obj2.handleCollision(obj1);
					if(sound)
						ticketSound.play();
					incClock(((TimeTicket) obj2).getTime());
					goItr.remove();
				}else if(obj2 instanceof SmartBomb){
					
					/* Smartbomb collision */
					obj2.handleCollision(obj1);
					if(sound)
						sbSound.play();
					resetCarLocation();
					incLivesLeft(GameWorld.DECREMENT);
					if(getLivesLeft() <= 0) gameOver(TIME_UP);
					goItr.remove();
				}
				alreadyCollided.add(obj2);
			}
		}	
		alreadyCollided.clear();
	}
	
	public void clockState(boolean tf){
		clockState = tf;
	}

	public void deleteSelected(){
		Iterator<GameObject> itr = gObjects.iterator();
		GameObject go;
		while(itr.hasNext()){
			go = (GameObject)itr.next();
			if(go instanceof ISelectable){
				if(((ISelectable) go).isSelected())
					itr.remove();
			}
		}
		notifyObservers();
	}
	
	/*
	 * The parameter passed in indicates
	 * the flag causing the execution of
	 * a gameover notification.
	 */
	public void gameOver(int y){
		int result = 1;
		if(y == TIME_UP){
			result = JOptionPane.showConfirmDialog(null, "Time is up!\n Want to restart?", "GAMEOVER", 
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);		
		}else if(y == NO_LIVES){
			result = JOptionPane.showConfirmDialog(null, "No more lives!\n Want to restart?", "GAMEOVER", 
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);	
		}
		
		if(result == JOptionPane.YES_OPTION){
			resetGame(INIT_LEVEL, getSoundState());
		}else{
			endTrack();
			System.exit(0);
		}
	}
	
	public void incClock(int incrementBy){ 
		gameClock += incrementBy; 
		notifyObservers();
		if(gameClock <=0)
			gameOver(TIME_UP);
	}
	
	public void incLivesLeft(int incrementBy){ 
		livesLeft += incrementBy; 
		notifyObservers();
	}
	
	public void incOwnedSqrs(int incrementBy){ 
		ownedSquares += incrementBy; 
		notifyObservers();
	}
	
	// Initialize playing field
	public void initField(){
		float locX, locY;
		int size = FIELD_UNIT_SIZE;	
		float min = 0;
		float max = FIELD_SIZE;
		
		// Top and bottom field squares
		for(int row = 0; row < 2; row++){
			for(int col = 1; col < 101; col++){
				locX = (col*size)+FIELD_SHIFT;
				locY = ((row == 0) ? min : max-size)+FIELD_SHIFT;
				// Edges
				fsObjects.add( new FieldSquare(locX, locY, OWNED));
			}
		}
		
		// Left and right side field squares
		for(int row = 1; row < 99; row++){
			for(int col = 0; col < 2; col++){
				locX = ((col == 0) ? min+size : max)+FIELD_SHIFT;
				locY = (row*size)+FIELD_SHIFT;
				// Edges
				fsObjects.add( new FieldSquare(locX, locY, OWNED));
			}
		}
		notifyObservers();
	}
	
	public void moveObjs(){
		Iterator<GameObject> goItr = gObjects.iterator();
		GameObject go;
		
		while(goItr.hasNext()){
			go = (GameObject)goItr.next();
			if(go instanceof Moveable){
				if(!(go instanceof Car))
					((Moveable) go).move(this);
			}
		}
		((Car)gObjects.at(THE_CAR)).move(this);
		notifyObservers();
	}
	
	public void newBall(){
		float [] xy = checkLocation(randomInt(FIELD_SIZE), 
					randomInt(FIELD_SIZE), MB_RADIUS, MB_RADIUS);
		
		addObject(new Monsterball(xy[0], xy[1],
					randColor(), randomInt(MAX_HEADING_DEG), randomInt(MAX_SPEED)+1, MB_RADIUS));
	}
	
	public void newTicket(int currentLevel){
		float [] xy = checkLocation(randomInt(FIELD_SIZE), 
				randomInt(FIELD_SIZE), TICKET_WIDTH, TICKET_HEIGHT);
		TimeTicket tt = new TimeTicket(xy[0], xy[1], randColor(), TICKET_WIDTH, TICKET_HEIGHT, 
				calcTicketTimer(currentLevel));
		addObject(tt);
	}
	
	public void newSmartBomb(){
		float [] xy = checkLocation(randomInt(FIELD_SIZE), randomInt(FIELD_SIZE), 
				SB_SIZE, SB_SIZE);
		SmartBomb sb = new SmartBomb(xy[0], xy[1], randColor(), randomInt(MAX_HEADING_DEG), 
					randomInt(MAX_SPEED)+1, SB_SIZE);
		sb.setStrategy(currentStrategy);
		addObject(sb);
	}
	
	public void newSquare(float x, float y, Color clr){
		fsObjects.add(new FieldSquare(x, y, clr));
	}

	public void resetCarLocation(){
		getCar().changeHeading(Moveable.SOUTH);
		((Car)gObjects.at(THE_CAR)).reset();
		notifyObservers();
	}
	
	public void resetGame(int currentLevel, boolean sound){	
		// Reset game states
		this.livesLeft = MAX_LIVES;
		this.currentLevel = currentLevel;
		this.minScore = calcMinScore();
		this.gameClock = initClock();
		this.ownedSquares = 0;
		setTicketTimes();
		
		/* Remove all field objects and reinitializes the field and
		  Reset car location*/
		reInitField();
		resetCarLocation();
	
		notifyObservers();
	}
	
	public void setSoundState(boolean state){ sound = state; }
	
	public void setClock(int newClock){ 
		gameClock = newClock; 
		notifyObservers();
	}
	
	public void setLivesLeft(int newLivesLeft){ livesLeft = newLivesLeft; }
	
	public void setLevel(int lvl){ currentLevel = lvl; }
	
	public void switchSBStrategy(){
		Iterator<GameObject> goItr = gObjects.iterator();
		GameObject go;
		// Choose new strategy
		while(goItr.hasNext()){
			go = (GameObject)goItr.next();
			if(go instanceof SmartBomb){
				((SmartBomb)go).switchStrategy();
			}
		}
		notifyObservers();
	}	
	
	public void switchSound(){
		sound = (sound) ? false : true;
		notifyObservers();
	}
} /* End GameWorld */