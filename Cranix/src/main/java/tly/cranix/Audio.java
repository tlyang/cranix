package tly.cranix;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import java.io.File;
import java.io.IOException;

public class Audio extends Thread{
	private final int BUFFER_SIZE = 64*1024;  // 64 KB
	private SourceDataLine soundLine;
	private AudioInputStream audioInputStream;
	private String soundPath;
	private volatile boolean suspended;
	private volatile boolean end;
	
	public Audio(String soundPath){
		this.soundPath = soundPath;
		soundLine = null;
		audioInputStream = null;
		end = false;
		suspended = false;
	}
	
	public boolean isActive(){
		return soundLine.isActive();
	}
	
	public void end(){
		soundLine.stop();
		soundLine.close();
		end = true;
	}
	
	public void pause(){
		soundLine.stop();
		suspended = true;
	}
	
	public synchronized void play(){
		soundLine.start();
		suspended = false;
		notify();
	}

	public void run() {
		while(!end){		
			try {
				/* Initialize audio stream line */
				File soundFile = new File(soundPath);
				audioInputStream = AudioSystem.getAudioInputStream(soundFile);
				AudioFormat audioFormat = audioInputStream.getFormat();
				DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
				soundLine = (SourceDataLine) AudioSystem.getLine(info);
				soundLine.open(audioFormat);
				soundLine.start();
				
				try {
					// Play audio
					int nBytesRead = 0;
					byte[] sampledData = new byte[BUFFER_SIZE];
					while (nBytesRead != -1) {
						
						// Play/pause thread
						synchronized(this){
							while(suspended){
								try {
									wait();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}
						
						nBytesRead = audioInputStream.read(sampledData, 0, sampledData.length);
						if (nBytesRead >= 0) {
							// Writes audio data to the mixer via this source data line.
							soundLine.write(sampledData, 0, nBytesRead);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (UnsupportedAudioFileException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			} catch (LineUnavailableException ex) {
				ex.printStackTrace();
			} finally {
				soundLine.drain();
				soundLine.close();
			}
		}	
	}
}
