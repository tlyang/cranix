package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableNull;

public class QuitCommand extends AbstractAction implements IEnableNull{
	GameWorld g;
	
	public QuitCommand(GameWorld g){
		super("Quit");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		int result;
		result = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit?", "Confirm exit", 
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(result == JOptionPane.YES_OPTION){
			g.endTrack();
			System.exit(0);
		}
	}
}