package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableTrue;
import tly.cranix.type.Moveable;

public class UpCommand extends AbstractAction implements IEnableTrue{
	GameWorld g;
	public UpCommand(GameWorld g){
		super("Up");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.getCar().changeHeading(Moveable.NORTH);
	}
}