package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;

public class SmartBombCollide extends AbstractAction{
	GameWorld g;
	public SmartBombCollide(GameWorld g){
		super("Smart Bomb Collision");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.incLivesLeft(GameWorld.DECREMENT);
		// Remove Smartbomb from the world
		g.resetCarLocation();
		if(g.getLivesLeft() <= 0)
			g.gameOver(0);
	}
}