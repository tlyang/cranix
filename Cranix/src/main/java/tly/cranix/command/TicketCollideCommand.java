package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;

public class TicketCollideCommand extends AbstractAction{
	GameWorld g;
	public TicketCollideCommand(GameWorld g){
		super("Car and time ticket collision");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		// No target ticket object yet so unable to remove from game world
		g.setClock(g.getClock() + g.getLevelTicketTime());
	}
}