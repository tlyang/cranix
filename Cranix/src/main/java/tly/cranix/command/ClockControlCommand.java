package tly.cranix.command;

import java.awt.event.ActionEvent;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.Timer;

import tly.cranix.CommandCollection;
import tly.cranix.GameWorld;
import tly.cranix.type.IEnableFalse;
import tly.cranix.type.IEnableTrue;
import tly.cranix.type.IEnabler;

public class ClockControlCommand extends AbstractAction {
	private int CLK_DELAY = 20;
	private GameWorld gw;
	private Timer t;
	private ClockCommand clkCmd;
	private CommandCollection cmds;
	
	public ClockControlCommand(GameWorld gw, CommandCollection cmds){
		super("Start");
		this.gw = gw;
		clkCmd = new ClockCommand(gw);
		t = new Timer(CLK_DELAY, clkCmd);
		gw.clockState(true);
		this.cmds = cmds;
		switchEnables(false);
	}
	
	// boolean value controls the commands available while
	//  the clock is running, while disabling the commands
	//  that shouldn't run at that time.
	private void switchEnables(boolean value){
		Iterator<IEnabler> itr = cmds.iterator();
		IEnabler act;
		
		while(itr.hasNext()){
			act = (IEnabler)itr.next();
			if(act instanceof IEnableTrue)	
				((AbstractAction)act).setEnabled(value);
			else if(act instanceof IEnableFalse)
				((AbstractAction)act).setEnabled(!value);
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if(t.isRunning()){
			// Pause the game
			t.stop();
			((JButton)e.getSource()).setText("Play");

			// Enable/Disable gui buttons/menus associated with commands
			switchEnables(false);
		}else{
			// Resume/Start the game
			t.start();
			((JButton)e.getSource()).setText("Pause");
			switchEnables(true);
		}
	}
}
