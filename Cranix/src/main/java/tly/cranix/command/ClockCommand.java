package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;

public class ClockCommand extends AbstractAction{
	public static final int DELAY = 50;
	private GameWorld gw;
	
	private static int delayNums;
	private static int sbCount;

	public ClockCommand(GameWorld gw){
		super("Tick");
		this.gw = gw;
		delayNums = 0;
		sbCount = 0;
	}
	
	public void actionPerformed(ActionEvent e){
		gw.moveObjs();
		gw.checkCollisions();
		/*
		 * delayNums increment to approximately one second 
		 */
		if(delayNums >= DELAY){
			delayNums = 0;
			
			/* 
			 * sbCount keeps track of the number of "seconds"
			 * that has passed by. At 10 seconds, smart bombs
			 * switch strategy and sb is reset. 
			 */			
			++sbCount;
			if(sbCount >= 10){
				gw.switchSBStrategy();
				sbCount = 0;
			}
			
			gw.incClock(GameWorld.DECREMENT);
		}
		
		++delayNums;
		
		gw.notifyObservers();
	}
}