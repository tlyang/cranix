package tly.cranix.command;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableTrue;

public class ZoomCommand implements IEnableTrue, MouseWheelListener {
	private final int ZOOM_MAX = 70;
	private boolean onEdge;
	private GameWorld gw;
	private int counter;
	
	public ZoomCommand(GameWorld gw){
		this.gw = gw;
		counter = 0;
		onEdge = false;
	}
	
	public void mouseWheelMoved(MouseWheelEvent e) {
		/* Only handle wheel scroll */
		if(e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL){
			int clicks = e.getWheelRotation();
			/*if(clicks > 0){*/
				/* Check for maximum zoom out/in */
				if(counter+1 < ZOOM_MAX && counter-1 > -ZOOM_MAX){
					if( !(counter+1 == ZOOM_MAX || counter-1 == -ZOOM_MAX) )
						counter += clicks;
					gw.setWinLeft(gw.getWinLeft() + clicks);
					gw.setWinRight(gw.getWinRight() - clicks);
					gw.setWinTop(gw.getWinTop() - clicks);
					gw.setWinBot(gw.getWinBot() + clicks);
				}
			/*}else{
				if(counter > -ZOOM_MAX){
					--counter;
					gw.setWinLeft(gw.getWinLeft() + clicks);
					gw.setWinRight(gw.getWinRight() - clicks);
					gw.setWinTop(gw.getWinTop() - clicks);
					gw.setWinBot(gw.getWinBot() + clicks);
				}
			}*/
		}	
		gw.notifyObservers();
	}
}
