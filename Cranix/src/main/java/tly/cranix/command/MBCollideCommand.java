package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;

public class MBCollideCommand extends AbstractAction{
	GameWorld g;
	public MBCollideCommand(GameWorld g){
		super("Car and monsterball collision");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.resetCarLocation();
		g.incLivesLeft(GameWorld.DECREMENT);
		// Check to see if there are no lives left
		if(g.getLivesLeft() <= 0)
			g.gameOver(0);
		// No monsterball object to change color yet
	}
}