package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import tly.cranix.type.IEnableNull;

public class AboutCommand extends AbstractAction implements IEnableNull{
	public AboutCommand(){
		super("About");
	}
	
	public void actionPerformed(ActionEvent e){
		JOptionPane.showMessageDialog(null,"Caronix by Thomas Yang\nCSC133 Spring 2014","About",JOptionPane.INFORMATION_MESSAGE);
	}
}