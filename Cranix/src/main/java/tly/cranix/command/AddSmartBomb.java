package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableFalse;

public class AddSmartBomb extends AbstractAction implements IEnableFalse{
	GameWorld g;
	
	public AddSmartBomb(GameWorld g){
		super("Add Smart Bomb");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.newSmartBomb();
	}
}