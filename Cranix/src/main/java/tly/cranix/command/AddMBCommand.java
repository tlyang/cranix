package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableFalse;

public class AddMBCommand extends AbstractAction implements IEnableFalse{
	private GameWorld gw;
	
	public AddMBCommand(GameWorld gw){
		super("Add Monsterball");
		this.gw = gw;
	}
	
	public void actionPerformed(ActionEvent e){
		gw.newBall();
	}
}