package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableFalse;

public class DeleteCommand extends AbstractAction implements IEnableFalse{
	GameWorld g;
	
	public DeleteCommand(GameWorld g) {
		super("Delete");
		this.g = g;
	}

	public void actionPerformed(ActionEvent e) {
		g.deleteSelected();
	}
}
