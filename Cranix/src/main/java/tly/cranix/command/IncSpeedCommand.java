package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.model.GameObject;
import tly.cranix.type.IEnableTrue;

public class IncSpeedCommand extends AbstractAction implements IEnableTrue{
	GameWorld g;
	public IncSpeedCommand(GameWorld g){
		super("Increment Speed");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.changeSpeed((GameObject)g.getCar(), GameWorld.INCREMENT);	
	}
}