package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.model.GameObject;
import tly.cranix.type.IEnabler;
import tly.cranix.type.Moveable;

public class LeftCommand extends AbstractAction implements IEnabler{
	GameWorld g;
	public LeftCommand(GameWorld g){
		super("Left");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.getCar().changeHeading(Moveable.WEST);
	}
}