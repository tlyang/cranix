package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.model.GameObject;
import tly.cranix.type.IEnableTrue;

public class DecSpeedCommand extends AbstractAction implements IEnableTrue { 
	GameWorld g;
	public DecSpeedCommand(GameWorld g){
		super("Decrement Speed");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e) {
		g.changeSpeed((GameObject)g.getCar(), GameWorld.DECREMENT);
	}	
}