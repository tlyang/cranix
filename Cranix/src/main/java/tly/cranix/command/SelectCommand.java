package tly.cranix.command;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.Iterator;

import javax.swing.JPanel;

import tly.cranix.GameWorld;
import tly.cranix.model.GameObject;
import tly.cranix.type.IDrawable;
import tly.cranix.type.IEnableFalse;
import tly.cranix.type.ISelectable;

public class SelectCommand extends JPanel implements MouseListener, MouseMotionListener, IEnableFalse{
	private GameWorld g;
	private Point2D prevPoint;
	private Point currentPoint;
	private AffineTransform theVTM;
	
	public SelectCommand(GameWorld g, AffineTransform theVTM){
		this.g = g;
		this.theVTM = theVTM;
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseDragged(MouseEvent e){}

	public void mousePressed(MouseEvent e) {
		Point screenPoint = e.getPoint();
		
		AffineTransform inverseVTM = new AffineTransform();
		
		try {
		   inverseVTM = theVTM.createInverse();
		} catch (NoninvertibleTransformException ni) {
			throw new RuntimeException(ni);
		}
		
		prevPoint = inverseVTM.transform(screenPoint,null);
		
		Iterator<GameObject> itr = g.getGameCollection().iterator();
		GameObject go;
		
		while(itr.hasNext()){
			go = (GameObject)itr.next();
			if(go instanceof ISelectable){
				if(((ISelectable) go).contains(prevPoint)){
					((ISelectable) go).setSelected(true);
				}else
					((ISelectable) go).setSelected(false);
			}
		}
		g.notifyObservers();
	}
	
	/*
	 * Draw a box to select game objects
	 */
	/*public void mouseDragged(MouseEvent e) {
		currentPoint = e.getPoint(); // get current mouse position 	
		//Graphics g = this.getGraphics();
		int dx = currentPoint.x - prevPoint.x;
		int dy = currentPoint.y - prevPoint.y;
		//g.setColor(Color.green);
		//g.drawRect(prevPoint.x, prevPoint.y, dx, dy);
	}*/
}
