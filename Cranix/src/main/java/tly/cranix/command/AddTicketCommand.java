package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableFalse;

public class AddTicketCommand extends AbstractAction implements IEnableFalse{
	GameWorld gw;
	
	public AddTicketCommand(GameWorld gw){
		super("Add Ticket");
		this.gw = gw;
		if(gw.getClockState())
			setEnabled(false);
	}
	
	public void actionPerformed(ActionEvent e){
		gw.newTicket(gw.getLevel());
	}
}