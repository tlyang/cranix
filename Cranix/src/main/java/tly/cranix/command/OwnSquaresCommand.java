package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableTrue;

public class OwnSquaresCommand extends AbstractAction implements IEnableTrue{
	GameWorld g;
	public OwnSquaresCommand(GameWorld g){
		super("Owned Squares");
		this.g = g;
	}

	public void actionPerformed(ActionEvent e) {
		int newSquares = GameWorld.randomInt(g.getNonOwnedSqrs());
		g.addOwnedSquares(newSquares);
		System.out.println("New acquired squares: " + newSquares);
		if(g.reachedMinScore()){
			JOptionPane.showMessageDialog(null, "LEVEL UP!");
			g.resetGame(g.getLevel()+1, g.getSoundState());
		}
	}
}