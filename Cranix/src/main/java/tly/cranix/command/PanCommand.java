package tly.cranix.command;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableTrue;

public class PanCommand implements MouseMotionListener, IEnableTrue, MouseListener {
	private int DRAG = 1;
	private GameWorld gw;
	private Point p0, init;
	private boolean grabbed;
	
	public PanCommand(GameWorld gw) {
		this.gw = gw;
	}

	public void mouseDragged(MouseEvent e) {
		/* save the current mouse location */
		p0 = e.getPoint();
		
		double dX = p0.getX()-init.getX();
		double dY = p0.getY()-init.getY();
				
		/* Calculate and save new window boundaries */
		gw.setWinLeft(gw.getWinLeft() - dX*DRAG);
		gw.setWinRight(gw.getWinRight() - dX*DRAG);
		gw.setWinTop(gw.getWinTop() + dY*DRAG);
		gw.setWinBot(gw.getWinBot() + dY*DRAG);
		
		/* save current point to use for next call */
		init = p0;
		
		if(grabbed) 
			gw.notifyObservers();
	}

	public void mousePressed(MouseEvent e) {
		init = e.getPoint();
		grabbed = true;
	}

	public void mouseReleased(MouseEvent e) {
		init = null;
		grabbed = false;
	}
/** Unused listener methods ***************************************************/
	public void mouseMoved(MouseEvent e) {}

	public void mouseClicked(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}
}
