package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableNull;

public class SoundCommand extends AbstractAction implements IEnableNull{
	GameWorld g;
	public SoundCommand(GameWorld g){
		super("Sound");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.switchSound();
		g.toggleTrack();
	}
}