package tly.cranix.command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import tly.cranix.GameWorld;
import tly.cranix.type.IEnableTrue;

public class IncSquareCommand extends AbstractAction implements IEnableTrue{
	GameWorld g;
	public IncSquareCommand(GameWorld g){
		super("Increment Field Square");
		this.g = g;
	}
	
	public void actionPerformed(ActionEvent e){
		g.incOwnedSqrs(GameWorld.INCREMENT);
		if(g.reachedMinScore()){
			g.resetGame(g.getLevel()+1, g.getSoundState());
			JOptionPane.showMessageDialog(null, "LEVEL UP!");
		}
	}
}