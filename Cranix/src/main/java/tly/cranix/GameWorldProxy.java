package tly.cranix;

import tly.cranix.model.Car;
import tly.cranix.type.IGameWorld;
import tly.cranix.type.IObservable;
import tly.cranix.type.IObserver;


public class GameWorldProxy implements IGameWorld, IObservable{
/* private constants */
	private static final int THE_CAR = 0;
	private static final int INIT_TICKET_TIME = 5;
	
/* attributes */
	private boolean clockState;
	private int currentLevel;
	private GameObjectCollection fieldObjects;
	private int gameClock;
	private GameObjectCollection gObjects;
	private ObserverCollection goObservers;
	private int livesLeft;
	private double minScore;	// a percentage
	private int ownedSquares;
	private boolean sound;
	
	/* Added in A4 */
	private double winTop, winRight, winLeft, winBot, winWidth, winHeight;

	
////////////////////////////////////////////////////////////////////////////////
		
/** Constructor ***************************************************************/	
	public GameWorldProxy(GameWorld g){
		goObservers = g.getObserverCollection();
		gObjects = g.getGameCollection();
		fieldObjects = g.getFieldCollection();
		livesLeft = g.getLivesLeft();
		gameClock = g.getClock();
		currentLevel = g.getLevel();
		minScore = g.getMinScore();
		sound = g.getSoundState();	
		ownedSquares = g.getOwnedSqrs();
		clockState = g.getClockState();
		winTop = g.getWinTop();
		winRight = g.getWinRight();
		winLeft = g.getWinLeft();
		winBot = g.getWinBot();
		winWidth = g.windowWidth();
		winHeight = g.windowHeight();
	}
	
/** Methods *******************************************************************/

/* IObserver methods */	
	public void addObserver(IObserver obs) {}
	
	public void notifyObservers() {
		goObservers.update(this);
	}

/* GameWorld Accessors */
	public int getNonOwnedSqrs(){
		return (int)GameWorld.MAX_SQUARES - ownedSquares;
	}
	
	public boolean reachedMinScore(){
		return (getScore() >= getMinScore()) ? true : false;
	}
	
	public Car getCar(){ return (Car)gObjects.at(THE_CAR); }
	
	public int getClock(){ return gameClock; }
	
	public boolean getClockState() { return clockState; }	
	
	public GameObjectCollection getFieldCollection(){ return fieldObjects; }

	public GameObjectCollection getGameCollection() {
		return gObjects;
	}
	
	public int getLivesLeft(){ return livesLeft; }

	public double getScore(){
		return 100 * (ownedSquares/GameWorld.MAX_SQUARES);
	}

	public int getLevel(){ return currentLevel; }
		
	public int getLevelTicketTime(){
		int time = INIT_TICKET_TIME - (getLevel() - 1);
		return (getLevel() >= 5) ? 1 : time;
	}
	
	public double getMinScore(){ return minScore; }
	
	public int getOwnedSqrs(){ return ownedSquares; }

	public ObserverCollection getObserverCollection(){ return goObservers; }
	
	public boolean getSoundState(){ return sound; }
	
	public double getWinRight(){ return winRight; }
	 
	public double getWinTop(){ return winTop; }
	
	public double getWinLeft(){ return winLeft; }
	
	public double getWinBot(){ return winBot; }
	
	public double windowWidth(){ return winWidth; }
	
	public double windowHeight(){ return winHeight; }
}