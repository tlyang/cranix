package tly.cranix;
import java.awt.*;

import javax.swing.*;

import tly.cranix.command.AboutCommand;
import tly.cranix.command.AddMBCommand;
import tly.cranix.command.AddSmartBomb;
import tly.cranix.command.AddTicketCommand;
import tly.cranix.command.ClockControlCommand;
import tly.cranix.command.DecSpeedCommand;
import tly.cranix.command.DeleteCommand;
import tly.cranix.command.DownCommand;
import tly.cranix.command.IncSpeedCommand;
import tly.cranix.command.IncSquareCommand;
import tly.cranix.command.LeftCommand;
import tly.cranix.command.OwnSquaresCommand;
import tly.cranix.command.QuitCommand;
import tly.cranix.command.RightCommand;
import tly.cranix.command.SoundCommand;
import tly.cranix.command.UpCommand;

//Controller class
public class Game extends JFrame{

	private GameWorld gw;
	private ScoreView sView;
	private MapView mView;
	
	//Commands
	private AboutCommand aboutCmd;
	private SoundCommand sndCmd;
	private AddTicketCommand newTicketCmd;
	private QuitCommand quitCmd;
	private AddMBCommand mbCmd;
	private IncSquareCommand incSqrCmd;
	private OwnSquaresCommand ownSqrsCmd;
	private AddSmartBomb newSBombCmd;
	private UpCommand upCmd;
	private DownCommand downCmd;
	private LeftCommand leftCmd;
	private RightCommand rightCmd;
	private IncSpeedCommand incSpeedCmd;
	private DecSpeedCommand decSpeedCmd;
	private DeleteCommand delCmd;
	private ClockControlCommand clkCntrlCmd;
	
	private JPanel centerPanel;
	private CommandCollection cmds;
	
	private int mapName;
	private ActionMap amap;
	private InputMap imap;
	
	KeyStroke upKey, leftKey, downKey, rightKey, iKey, lKey, spaceKey, sKey;

	private Dimension idealDim;

// Constructor
	public Game(){
		gw = new GameWorld();
		sView = new ScoreView();
		mView = new MapView(gw, new GameWorldProxy(gw));
		gw.addObserver(sView);
		gw.addObserver(mView);
		gw.notifyObservers();
		centerPanel = new JPanel(new BorderLayout());
		idealDim = new Dimension(875, 775);
		
		cmds = new CommandCollection();
		
		//Command initialization
		aboutCmd = new AboutCommand();
		sndCmd = new SoundCommand(gw);
		newTicketCmd = new AddTicketCommand(gw);
		quitCmd = new QuitCommand(gw);
		mbCmd = new AddMBCommand(gw);
		incSqrCmd = new IncSquareCommand(gw);
		ownSqrsCmd = new OwnSquaresCommand(gw);
		newSBombCmd = new AddSmartBomb(gw);
		upCmd = new UpCommand(gw);
		downCmd = new DownCommand(gw);
		leftCmd = new LeftCommand(gw);
		rightCmd = new RightCommand(gw);
		incSpeedCmd = new IncSpeedCommand(gw);
		decSpeedCmd = new DecSpeedCommand(gw);
		delCmd = new DeleteCommand(gw);
		
		// Add to collection
		cmds.add(aboutCmd);
		cmds.add(sndCmd);
		cmds.add(newTicketCmd);
		cmds.add(newSBombCmd);
		cmds.add(quitCmd);
		cmds.add(mbCmd);
		cmds.add(incSqrCmd);
		cmds.add(ownSqrsCmd);
		cmds.add(upCmd);
		cmds.add(downCmd);
		cmds.add(leftCmd);
		cmds.add(rightCmd);
		cmds.add(incSpeedCmd);
		cmds.add(decSpeedCmd);
		cmds.add(delCmd);
		
		clkCntrlCmd = new ClockControlCommand(gw, cmds);
		
		// Draw GUI
		drawGUI();
		
		// Keybinding map initialization.
		mapName = JComponent.WHEN_IN_FOCUSED_WINDOW;
		imap = centerPanel.getInputMap(mapName);
		
		// Declare Keystroke variable
		upKey = KeyStroke.getKeyStroke("UP");
		leftKey = KeyStroke.getKeyStroke("LEFT");
		downKey = KeyStroke.getKeyStroke("DOWN");
		rightKey = KeyStroke.getKeyStroke("RIGHT");
		iKey = KeyStroke.getKeyStroke('i');
		lKey = KeyStroke.getKeyStroke('l');
		spaceKey = KeyStroke.getKeyStroke("SPACE");
		sKey = KeyStroke.getKeyStroke("s");
		
		// Input map
		imap.put(upKey, "Up");
		imap.put(leftKey, "Left");
		imap.put(downKey, "Down");
		imap.put(rightKey, "Right");
		imap.put(iKey, 'i');
		imap.put(lKey, 'l');
		imap.put(spaceKey, "Space");
		imap.put(sKey, 's');
		
		// Put into panel's Action map
		amap = centerPanel.getActionMap();
		// Up and Down buttons are reversed for this program
		amap.put("Up", upCmd);
		amap.put("Down", downCmd);
		amap.put("Left", leftCmd);
		amap.put("Right", rightCmd);
		amap.put('i', incSpeedCmd);
		amap.put('l', decSpeedCmd);
		
		this.requestFocus();
	}

// Draw components of the GUI
	private void drawGUI(){
		setTitle("Caronix");
		setSize(idealDim);
		setMinimumSize(idealDim);
		setMaximumSize(idealDim);
		setLocation(100, 0);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// draw components
		centerPanel.add(drawCommandPanel(), BorderLayout.WEST);
		centerPanel.add(mView.getMapPanel(), BorderLayout.CENTER);
		centerPanel.add(sView.getScorePanel(), BorderLayout.NORTH);
		this.setJMenuBar(drawMenuBar());
		this.add(centerPanel, BorderLayout.CENTER);
		
		setVisible(true);	
	}
	
// Draw menu bar
	private JMenuBar drawMenuBar(){
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem item = new JMenuItem("New");
		JMenu cmdMenu = new JMenu("Commands");

		// File menu
		fileMenu.add(item);
		item = new JMenuItem("Save");
		fileMenu.add(item);
		item = new JMenuItem("Undo");
		fileMenu.add(item);
		// Sound checkbox
		JCheckBoxMenuItem sound = new JCheckBoxMenuItem("Sound");
		sound.setAction(sndCmd);
		sound.setState(gw.getSoundState());
		fileMenu.add(sound);
		item = new JMenuItem("About");
		item.setAction(aboutCmd);
		fileMenu.add(item);
		item = new JMenuItem("Quit");
		item.setAction(quitCmd);
		fileMenu.add(item);	
		
		// Commands menu
		item = new JMenuItem("Add Monsterball");
		item.setAction(mbCmd);
		cmdMenu.add(item);
		item = new JMenuItem("Add Time ticket");
		item.setAction(newTicketCmd);
		cmdMenu.add(item);
		item = new JMenuItem("Add Smartbomb");
		item.setAction(newSBombCmd);
		cmdMenu.add(item);

		item = new JMenuItem("Increment Fieldsquare");
		item.setAction(incSqrCmd);
		cmdMenu.add(item);
		item = new JMenuItem("Owned Squares");
		item.setAction(ownSqrsCmd);
		cmdMenu.add(item);
		
		menuBar.add(fileMenu);
		menuBar.add(cmdMenu);
		
		return menuBar;
	}
	
	// Draw panel that holds command buttons
	private JPanel drawCommandPanel(){
		JPanel cmdPanel = new JPanel();
		cmdPanel.setLayout(new GridLayout(11, 1));
		cmdPanel.setBorder(BorderFactory.createTitledBorder("Command"));

		//Buttons
		JButton tickButton = new JButton("Start");
		tickButton.setAction(clkCntrlCmd);
		tickButton.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),"none");
		
		JButton addMBButton = new JButton("Add Monsterball");
		addMBButton.setAction(mbCmd);
		addMBButton.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),"none");
		
		JButton addTicketButton = new JButton("Add Timeticket");
		addTicketButton.setAction(newTicketCmd);
		addTicketButton.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),"none");
		
		JButton addSmartBomb = new JButton("Add Smartbomb");
		addSmartBomb.setAction(newSBombCmd);
		addSmartBomb.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),"none");
		
		JButton addDelButton = new JButton("Delete");
		addDelButton.setAction(delCmd);
		addDelButton.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "none");

		JButton quitButton = new JButton("Quit");
		quitButton.setAction(quitCmd);
		quitButton.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),"none");
		
		cmdPanel.add(tickButton);
		cmdPanel.add(addMBButton);
		cmdPanel.add(addTicketButton);
		cmdPanel.add(addSmartBomb);
		cmdPanel.add(addDelButton);
		cmdPanel.add(quitButton);
		return cmdPanel;
	}	
}