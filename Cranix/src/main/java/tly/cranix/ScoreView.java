package tly.cranix;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.*;

import tly.cranix.type.IObserver;

public class ScoreView extends JPanel implements IObserver{
	private JLabel currLevelLabel;
	private JLabel currScoreLabel;
	private JLabel livesLeftLabel;
	private JLabel minScoreLabel;
	private JLabel soundLabel;
	private JLabel timeLabel;
		
	public ScoreView(){
		currLevelLabel = new JLabel();
		currScoreLabel = new JLabel();
		livesLeftLabel = new JLabel();
		minScoreLabel = new JLabel();
		soundLabel = new JLabel();
		timeLabel = new JLabel();
		drawScorePanel();
	}

	public JPanel getScorePanel(){ return this; }
	
	public void drawScorePanel(){
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		this.setLayout(new GridLayout(1, 6, 20, 1));
		this.setMinimumSize(new Dimension(850, 40));
		this.setMaximumSize(new Dimension(850, 40));
		
		JLabel currentLevel = new JLabel("Current Level: ");
		JLabel timeLeft = new JLabel("Time: ");
		JLabel livesLeft = new JLabel("Lives: ");
		JLabel targetScore = new JLabel("Target Score: ");
		JLabel score = new JLabel("Score: ");
		JLabel sound = new JLabel("Sound: ");

		currentLevel.setHorizontalAlignment(SwingConstants.RIGHT);
		timeLeft.setHorizontalAlignment(SwingConstants.RIGHT);
		livesLeft.setHorizontalAlignment(SwingConstants.RIGHT);
		score.setHorizontalAlignment(SwingConstants.RIGHT);
		targetScore.setHorizontalAlignment(SwingConstants.RIGHT);
		sound.setHorizontalAlignment(SwingConstants.RIGHT);
		
		currLevelLabel.setHorizontalAlignment(SwingConstants.LEFT);
		timeLabel.setHorizontalAlignment(SwingConstants.LEFT);
		livesLeftLabel.setHorizontalAlignment(SwingConstants.LEFT);
		currScoreLabel.setHorizontalAlignment(SwingConstants.LEFT);
		minScoreLabel.setHorizontalAlignment(SwingConstants.LEFT);
		soundLabel.setHorizontalAlignment(SwingConstants.LEFT);
		
		JPanel levelP = new JPanel();
		levelP.add(currentLevel);
		levelP.add(currLevelLabel);
		this.add(levelP);
		
		JPanel timeP = new JPanel();
		timeP.add(timeLeft);
		timeP.add(timeLabel);
		this.add(timeP);
		
		JPanel livesP = new JPanel();
		livesP.add(livesLeft);
		livesP.add(livesLeftLabel);
		this.add(livesP);
		
		
		JPanel scoreP = new JPanel();
		scoreP.add(score);
		scoreP.add(currScoreLabel);
		this.add(scoreP);
		
		JPanel targetP = new JPanel();
		targetP.add(targetScore);
		targetP.add(minScoreLabel);
		this.add(targetP);
		
		JPanel soundP = new JPanel();
		soundP.add(sound);
		soundP.add(soundLabel);
		this.add(soundP);
	}
	
	public void update(GameWorldProxy gwp){
		String soundText = new String();
		String scoreStr = new String();
		
		currLevelLabel.setText(Integer.toString(gwp.getLevel()));
		timeLabel.setText(Integer.toString(gwp.getClock()));
		livesLeftLabel.setText(Integer.toString(gwp.getLivesLeft()));
		
		scoreStr = String.format("%.2f%%", gwp.getScore());
		currScoreLabel.setText(scoreStr);
		
		scoreStr = String.format("%.2f%%", gwp.getMinScore());
		minScoreLabel.setText(scoreStr);
		
		soundText = gwp.getSoundState() ? "On" : "Off";
		soundLabel.setText(soundText);	
	}
}