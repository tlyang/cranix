package tly.cranix;

import java.util.ArrayList;
import java.util.Iterator;

import tly.cranix.type.IEnabler;

public class CommandCollection implements Iterable<IEnabler> {
	private ArrayList<IEnabler> cmdCollection;
	
	private class CMDIterator implements Iterator{
		private int current;

		public CMDIterator(){
			current = -1;
		}
		
		public boolean hasNext() {
			if(cmdCollection.size() <= 0) return false;
			if(current == cmdCollection.size()-1) return false;
			return true;
		}

		public IEnabler next() {
			current++;
			return cmdCollection.get(current);
		}

		public void remove() {}
	}
	
	public CommandCollection(){
		cmdCollection = new ArrayList<IEnabler>();
	}
	
	public CommandCollection(int capacity){
		cmdCollection = new ArrayList<IEnabler>(capacity);
	}
	
	public IEnabler at(int index){
		return cmdCollection.get(index);
	}
	
	public void add(IEnabler g) {
		cmdCollection.add(g);
	}
	
	public Iterator<IEnabler> iterator() {
		return new CMDIterator();
	}

}
