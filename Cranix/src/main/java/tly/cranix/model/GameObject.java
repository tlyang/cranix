package tly.cranix.model;

import java.awt.Color;

import tly.cranix.GameWorld;
import tly.cranix.type.IChangeableColor;

public abstract class GameObject{
// Variable
	private Color clr;
	private Location location;
	
// Constructors
	public GameObject(Color newClr, Location location){
		clr = newClr;
		this.location = location;
	}
		
// Public Static Methods
	public static int randInt(int upperBound){
		return GameWorld.randomInt(upperBound);
	}
	
	public static float randFloat(int upperBound){
		return GameWorld.randomFloat(upperBound);
	}

// Public Methods	
	public String toString(){
		return clr.toString() + " ";
	}
	
	public void setColor(Color newClr){
		if(this instanceof IChangeableColor)
			clr = newClr;
	}
	
	public abstract int getSize();

	public Color getColor(){ 
		return clr; 
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
}