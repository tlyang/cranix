package tly.cranix.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.util.Iterator;

import tly.cranix.GameWorld;
import tly.cranix.command.ClockCommand;
import tly.cranix.type.ICollider;
import tly.cranix.type.IDrawableModifiable;
import tly.cranix.type.ISteerable;
import tly.cranix.type.Moveable;


public class Car extends Moveable implements ISteerable, IDrawableModifiable, ICollider{
	private int width;
	private int height;
	private double x, y;
	private AffineTransform myTranslation, myRotation;
	
	public Car(double init_x, double init_y, Color clr, int newWidth, int newHeight, int newHeading, int newSpeed){
		// Location is set to center of car
		super(clr, newHeading, newSpeed, new Location((float)init_x, (float)init_y));
		width = newWidth;
		height = newHeight;
		x = -width/2;
		y = height/2;
		
		myTranslation = new AffineTransform();
		myRotation = new AffineTransform();
		translate(init_x, init_y);
	}	
	
	/* Check to see if the car is currently on the boundary */
	private boolean checkEdge(double x, double y){
		float unitSize = GameWorld.FIELD_UNIT_SIZE;
		float fieldSize = GameWorld.FIELD_SIZE;
		float shift = GameWorld.FIELD_SHIFT;

		if((x <= unitSize + shift) || (x >= fieldSize + shift) || 
				(y <= shift) || (y >= fieldSize-unitSize+shift)){
			return true;
		}
		return false;
	}
	
	public int getWidth(){ return width; }
	
	public int getHeight(){ return height; }
	
	public int getSize(){
		return (width < height) ? width : height;
	}

	public void reset(){
		/* Reset transforms */
		myTranslation.setToIdentity();
		myRotation.setToIdentity();
		/* Translate to starting location */
		translate(GameWorld.START_X, GameWorld.START_Y);
	}
	
	/** ISteerable ************************************************************/
	public void changeHeading(double newHeading){ 
		setHeading(newHeading);
		rotate(90-newHeading);
	}
	
	/** Moveable **************************************************************/
	public void move(GameWorld gw){
		int fieldSize = GameWorld.FIELD_SIZE;
		float unitSize = GameWorld.FIELD_UNIT_SIZE;
		float shift = GameWorld.FIELD_SHIFT;
		
		double oldX = myTranslation.getTranslateX();
		double oldY = myTranslation.getTranslateY();
		double dX = 0;
		double dY = 0;
		double newX, newY;
		
		dX = (Math.cos(Math.toRadians(90-getHeading()))*getSpeed() );
		dY = (Math.sin(Math.toRadians(90-getHeading()))*getSpeed());
		
		
		newX = oldX+dX;
		newY = oldY+dY;
		
		/* Stops the car if it hits the border */
		if(checkEdge(newX, newY)){
			/* Refuse movement to go outside 	
			 *  if the car is on the boundary 	*/
			if( (getHeading() == Moveable.WEST) && (newX < unitSize + shift)  || 
					(getHeading() == Moveable.EAST) && (newX> fieldSize + shift) ){
				dX = 0;
			}else{
				if((newY < shift) || (newY > fieldSize-unitSize+shift))
					dY=0;
			}
		}
		
		translate(dX, dY);

		// Check for existence of a field square with the new location
		Iterator<GameObject> fsItr = gw.getFieldCollection().iterator();
		FieldSquare fs;
		boolean newLocOwned = false;
		double fs_x = myTranslation.getTranslateX();
		double fs_y = myTranslation.getTranslateY();
		
		while(fsItr.hasNext()){
			fs = (FieldSquare)fsItr.next();
			fs_x = fs.getTranslation().getTranslateX();
			fs_y = fs.getTranslation().getTranslateY();
			
			if(fs_x == newX && fs_y == newY){
				// Existing field square found at the location
				newLocOwned = true;
				break;
			}
		}
		
		// If no field square was found, create one
		if(!newLocOwned && !checkEdge(newX, newY)){
			//if(newX % unitSize == 0 && newY % unitSize == 0)
				gw.newSquare((float)newX, (float)newY, GameWorld.MAYBE_OWNED);
		}
		
		// If old location was in the field and new location is
		//  on the border, successfully traversed the field
		if(!checkEdge(oldX, oldY) && checkEdge(newX, newY)){
			fsItr = gw.getFieldCollection().iterator();
			while(fsItr.hasNext()){
				fs = (FieldSquare)fsItr.next();
				if(fs.getColor() == GameWorld.MAYBE_OWNED)
					fs.setColor(GameWorld.OWNED);
			}
		}
	}
	
	/** IDrawable *************************************************************/
	public void draw(Graphics2D g2d){
		AffineTransform saveAT = g2d.getTransform();
		
		g2d.transform(myTranslation);
		
		g2d.setColor(getColor());
		g2d.fill3DRect((int)x-width/2, (int)y-height/2, width, height, true);
		g2d.setColor(Color.BLACK);
		//g2d.drawRect((int)x, (int)y, width, height);
		g2d.drawRect((int)x, (int)y, 1, 1);
		g2d.setTransform(saveAT);
	}
	
	public AffineTransform getTranslation() {
		double x = myTranslation.getTranslateX();
		double y = myTranslation.getTranslateY();
		return AffineTransform.getTranslateInstance(x, y);
		// return (AffineTransform)myTranslation.clone();
	}
	
	public void rotate(double d) {
		myRotation.rotate(d);
	}
	
	public void scale(int amt) {

	}
	
	public void translate(double init_x, double init_y){
		myTranslation.translate(init_x,  init_y);
	}
	
	/** ICollider *************************************************************/
	public boolean collidesWith(ICollider obj){
		GameObject gObj = (GameObject)obj;
		float carX = getLocation().getX()-width/2;
		float carY = getLocation().getY()-height/2;
		
		float objX = gObj.getLocation().getX();
		float objY = gObj.getLocation().getY();

		float carXR = carX + width;
		float carXL = carX;
		float carYT = carY;
		float carYB = carY + height;
		
		float objXR = objX + gObj.getSize();
		float objXL = objX;
		float objYT = objY;
		float objYB = objY + gObj.getSize();
		
		if((carXL < objXR) && (carXR > objXL) && (carYT < objYB) && (objYT < carYB))
			return true;
		return false;
	}

	public void handleCollision(ICollider obj){}
}