package tly.cranix.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

import tly.cranix.GameWorld;
import tly.cranix.command.ClockCommand;
import tly.cranix.strategy.ChaseStrategy;
import tly.cranix.strategy.DefaultStrategy;
import tly.cranix.strategy.ISmartBombStrategy;
import tly.cranix.type.ICollider;
import tly.cranix.type.IDrawableModifiable;
import tly.cranix.type.ISelectable;
import tly.cranix.type.ISteerable;
import tly.cranix.type.Moveable;

public class SmartBomb extends Moveable implements IDrawableModifiable, ICollider, ISelectable, ISteerable{
	/* The amount in degrees the bomb will rotate at each call to move() */
	private final int TUMBLE = 67;
	private ISmartBombStrategy strategy;
	private final int radius;
	private int dirX, dirY;
	private boolean selected;
	private AffineTransform myRotation, myTranslation, myTransform;
	private Growth [] gs;
	
	public SmartBomb(float init_x, float init_y, Color clr, int heading, int speed, int size){
		super(clr, heading, speed, new Location(init_x, init_y));
		myRotation = new AffineTransform();
		myTranslation = new AffineTransform();
		this.radius = size/2;
		
		//gs = new Growth[4];
		
//		for(int i = 0; i < gs.length; i++){
//			gs[i] = new Growth();
//		}
		
		dirX = 1;
		dirY = 1;
		myTranslation.translate(init_x, init_y);
	}
	
	public int getSize(){
		return radius*2;
	}
	
	/** ISteerable ************************************************************/
	public void changeHeading(double theta) {
		setHeading(theta);
	}
	
	/** IDrawable *************************************************************/

	public void draw(Graphics2D g2d){
		/*
		 * If the smart bomb is in the chase strategy,
		 * it will turn red.
		 */
		AffineTransform saveAT = g2d.getTransform();
		
		// rotateSB();
		g2d.transform(myTranslation);
		g2d.transform(myRotation);
		
		/* Set color depending on strategy */
		if(strategy.strategy() == ISmartBombStrategy.CHASE)
			g2d.setColor(GameWorld.CHASE);
		else
			g2d.setColor(getColor());
		
		if(selected)
			g2d.setColor(GameWorld.SELECTED_COLOR);
		
		g2d.fillOval(-radius/2, -radius/2, radius*2, radius*2);
//		for(Growth gts: gs){
//			gts.draw(g2d);
//		}
		
		myTransform = g2d.getTransform();
		g2d.setTransform(saveAT);
	}
	
	public AffineTransform getTranslation() {
		double x = myTranslation.getTranslateX();
		double y = myTranslation.getTranslateY();
		return AffineTransform.getTranslateInstance(x, y);
	}

	public void rotate(double theta) {
		myRotation.rotate(theta);
	}
	
	public void scale(int amt){}
	
	public void translate(double x, double y) {
		myTranslation.translate(x, y);
	}

	/** IStrategy *************************************************************/
	public void setStrategy(ISmartBombStrategy strat){
		this.strategy = strat;
	}
	
	// Returns a string value of the current strategy 
	public String getStrategy(){
		String rt = new String();
		if(strategy instanceof DefaultStrategy)
			rt = "Default";
		else
			rt = "Chase";
		return rt;
	}
	
	public ISmartBombStrategy getBehavior(){ return strategy; }

	public void switchStrategy(){
		if(strategy.strategy() == ISmartBombStrategy.CHASE)
			strategy = new DefaultStrategy();
		else
			strategy = new ChaseStrategy();
	}
	
	/** Moveable **************************************************************/
	public void move(GameWorld gw){
		int behavior = strategy.strategy();
		int fieldSize = GameWorld.FIELD_SIZE;
		int unitSize = GameWorld.FIELD_UNIT_SIZE;
		int shift = GameWorld.FIELD_SHIFT;
		double myX = getTranslation().getTranslateX();
		double myY = getTranslation().getTranslateY();
		double carX = gw.getCar().getTranslation().getTranslateX();
		double carY = gw.getCar().getTranslation().getTranslateY();
		double theta = 90-getHeading();
		double deltaX, deltaY;
		
		/* If Chase strategy is active, change heading to head towards car */
		if(behavior == ISmartBombStrategy.CHASE){
			/* Chase strategy */
			deltaX = carX - myX;
			deltaY = carY - myX;
			theta = (float)Math.atan2(deltaX, deltaY)/ClockCommand.DELAY;
			/* Head straight to the car */
			changeHeading(theta);
		}
		
		/* 
		 * 	Default strategy
		 *  Multiply by dirX/dirY for positive or negative direction 
		 */
		deltaX = (dirX*Math.cos(theta)*(getSpeed()));
		deltaY = (dirY*Math.sin(theta)*(getSpeed()));
		
		translate(deltaX, deltaY);
		
//		for(Growth gts: gs){
//			int pos = GameWorld.randomInt(360)*gts.getSize();
//			gts.translate(Math.cos(pos), Math.sin(pos));
//		}
		
		rotate(TUMBLE);
//		for(Growth gts: gs){
//			gts.move(TUMBLE);
//		}
		
		double newX = myX + deltaX;
		double newY = myY + deltaY;
		
		// Bounce
		if(newX+radius > fieldSize+shift-unitSize/2 || newX < (1.5*unitSize)+shift){ 
			dirX = -dirX;
		}
		if(newY+radius > fieldSize-(1.5*unitSize)+shift || newY < shift+unitSize/2){ 
			dirY = -dirY;
		}
	}

	/** ICollider *************************************************************/
	public boolean collidesWith(ICollider obj) {
		return false;
	}

	public void handleCollision(ICollider obj) {
		try {
			Thread.sleep(GameWorld.COLLIDE_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/** ISelectable ***********************************************************/
	public void setSelected(boolean yn) {
		selected = yn;
	}

	public boolean isSelected() {
		return selected;
	}

	public boolean contains(Point2D p) {
		Point2D localPoint;
		/* Change from world point p to local point */
		try {
			localPoint = myTransform.createInverse().transform(p, null);
		} catch (NoninvertibleTransformException e) {
			throw new RuntimeException(e);
		}
		
		float localPointx = (float)localPoint.getX();
		float localPointy = (float)localPoint.getY();
		
		if( (localPointx >= -radius/2) && (localPointx <= radius/2) &&
				(localPointy >= -radius/2) && (localPointy <= radius/2))
			return true;
		else
			return false;
	}
}