package tly.cranix.model;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;

public class Sound {
	AudioClip myClip;
	
	public Sound(String filename){
		try{
			File file = new File(filename);
			if(file.exists()){
				myClip = Applet.newAudioClip(file.toURI().toURL());
			}else{
				throw new RuntimeException("Sound: file not found:" + filename);
			}
		}catch (MalformedURLException e){
			throw new RuntimeException("Sound: malformed URL: " + e);
		}
	}
	
	public void play(){
		myClip.play();
	}	
	
	public void stop(){
		myClip.stop();
	}
	
	public void loop(){
		myClip.loop();
	}
}
