package tly.cranix.model;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import tly.cranix.GameWorld;
import tly.cranix.type.IDrawableModifiable;

public class Growth implements IDrawableModifiable {
	private int sx, sy;
	private int WIDTH = 10;
	private int MAX_HEIGHT = 40;
	private int MAX_ANGLE = 89;
	private AffineTransform myRotation, myTranslation;
	
	public Growth() {
		sx = sy = 0;
	}

	public int getSize(){ return WIDTH; }
	public void move(int deg){
		
	}
	
	public void draw(Graphics2D g2d) {
		g2d.fillArc(sx, sy, WIDTH, GameWorld.randomInt(MAX_HEIGHT), GameWorld.randomInt(MAX_ANGLE), 180);
	}

	public AffineTransform getTranslation() {
		return null;
	}

	public void rotate(double theta) {
		myRotation.rotate(theta);
	}

	public void translate(double x, double y) {
		myTranslation.translate(x, y);
	}

	public void scale(int amt) {}

}
