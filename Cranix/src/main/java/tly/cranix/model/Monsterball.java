package tly.cranix.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import tly.cranix.GameWorld;
import tly.cranix.type.IChangeableColor;
import tly.cranix.type.ICollider;
import tly.cranix.type.IDrawableModifiable;
import tly.cranix.type.Moveable;

public class Monsterball extends Moveable implements IChangeableColor, IDrawableModifiable, ICollider{
	private final int radius;
	private int dirX;
	private int dirY;
	private AffineTransform myTranslation;
	private double x, y;

	public Monsterball(double init_x, double init_y, Color clr, int heading, int speed, int radius){
		super(clr, heading, speed, new Location((float)init_x, (float)init_y));
		myTranslation = new AffineTransform();
		/* Set location */
		this.radius = radius; 
		x = -radius;
		y = radius;
		
		/* Initialize X and Y direction */
		dirX = 1;
		dirY = 1;
		
		/* Translate to initial position */
		myTranslation.translate(init_x, init_y);
	}
	
	// Returns diameter of Monsterball
	public int getSize(){
		return radius;
	}
	
	/** IDrawable *************************************************************/
	public AffineTransform getTranslation(){
		double x = myTranslation.getTranslateX();
		double y = myTranslation.getTranslateY();
		return AffineTransform.getTranslateInstance(x, y);
	}

	public void rotate(double theta) {}

	public void scale(int amt) {}
	
	public void translate(double init_x, double init_y){
		myTranslation.translate(init_x, init_y);
	}
	
	public void changeColor(Color clr){ setColor(clr); }
	
	public void draw(Graphics2D g2d){
		/* Save current transform */
		AffineTransform saveAT = g2d.getTransform();
		g2d.transform(myTranslation);
		g2d.setColor(getColor());
		g2d.fillOval((int)x, (int)y, radius*2, radius*2);
		g2d.setTransform(saveAT);
	}
	
	/** Moveable **************************************************************/
	public void move(GameWorld gw){
		int fieldSize = GameWorld.FIELD_SIZE;
		int unitSize = GameWorld.FIELD_UNIT_SIZE;
		int shift = GameWorld.FIELD_SHIFT;
		double dX = 0;
		double dY = 0;
		double theta = 90-getHeading();
		double newX, newY;
		
		// Multiply by dirX/dirY for positive or negative direction
		dX = (dirX* Math.cos(theta)*getSpeed());
		dY = (dirY*Math.sin(theta)*getSpeed());
		
		newX = dX + getTranslation().getTranslateX();
		newY = dY + getTranslation().getTranslateY();
		
		// Translate
		translate(dX, dY);
		
		// Bounce
		if(newX+radius > fieldSize+shift-unitSize/2 || newX < (1.5*unitSize)+shift){ 
			dirX = -dirX;
		}
		if(newY+radius > fieldSize-(1.5*unitSize)+shift || newY < shift+unitSize/2){ 
			dirY = -dirY;
		}
	}
	
	/** ICollider *************************************************************/
	public boolean collidesWith(ICollider car){
		GameObject gObj = (GameObject)car;
		float mbX = getLocation().getX();
		float mbY = getLocation().getY();
		
		float objX = gObj.getLocation().getX();
		float objY = gObj.getLocation().getY();

		float mbXR = mbX;
		float mbXL = mbX;
		float mbYT = mbY;
		float mbYB = mbY;
		
		float objXR = objX + ((Car)gObj).getWidth();
		float objXL = objX;
		float objYT = objY;
		float objYB = objY + ((Car)gObj).getHeight();
		
		boolean xO = true;
		boolean yO = true;
		
		if((mbXR < objXL) || (mbXL > objXR)){
			xO = false;
		}
		if((objYT < mbYB) || (mbYT < objYB)){
			yO = false;
		}
		return (xO && yO) ? true : false;
	}
	
	public void handleCollision(ICollider obj){
		try {
			Thread.sleep(GameWorld.COLLIDE_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}