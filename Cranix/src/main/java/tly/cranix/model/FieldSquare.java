package tly.cranix.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import tly.cranix.Fixed;
import tly.cranix.GameWorld;
import tly.cranix.type.IChangeableColor;
import tly.cranix.type.ICollider;
import tly.cranix.type.IDrawable;

public class FieldSquare extends GameObject implements Fixed, IChangeableColor, IDrawable, ICollider{
	private final int size = GameWorld.FIELD_UNIT_SIZE;
	private AffineTransform myTranslation;
	private double x, y;
	
	public FieldSquare(float init_x, float init_y, Color clr){
		super(clr, new Location(init_x, init_y));
		x = -size/2;
		y = size/2;
		myTranslation = new AffineTransform();
		myTranslation.translate(init_x, init_y);
	}
	
	public int getSize(){
		return size;
	}
	
	/** IChangeableColor ******************************************************/
	public void changeColor(Color clr) {
		setColor(clr);
	}

	/** getTranslation ********************************************************/
	public AffineTransform getTranslation(){
		double x = myTranslation.getTranslateX();
		double y = myTranslation.getTranslateY();
		return AffineTransform.getTranslateInstance(x, y);
	}
	
	/** IDrawable *************************************************************/
	public void draw(Graphics2D g2d){
		AffineTransform saveAT = g2d.getTransform();
		g2d.transform(myTranslation);
		
		g2d.setColor(getColor());
		g2d.fillRect((int)x-size/2, (int)y-size/2, size, size);
		g2d.setColor(Color.BLACK);
		g2d.drawRect((int)x-size/2, (int)y-size/2, size, size);
		
		g2d.setTransform(saveAT);
	}

	/** ICollider *************************************************************/
	public boolean collidesWith(ICollider obj) {
		return false;
	}

	public void handleCollision(ICollider obj) {}
}