package tly.cranix.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

import tly.cranix.Fixed;
import tly.cranix.GameWorld;
import tly.cranix.type.ICollider;
import tly.cranix.type.IDrawable;
import tly.cranix.type.ISelectable;

public class TimeTicket extends GameObject implements Fixed, IDrawable, ICollider, ISelectable{
	private int time;
	private final int width;
	private final int height;
	private boolean selected;
	private double x, y;
	private AffineTransform myTranslation, myTransform;
	
	public TimeTicket(float init_x, float init_y, Color clr, int newWidth, int newHeight, int newTime){
		super(clr, new Location(init_x, init_y));
		x = init_x;
		y = init_y;
		width = newWidth;
		height = newHeight;
		time = newTime;
		selected = false;
		
		myTranslation = new AffineTransform();
		myTranslation.translate(x, y);
	}
	
	public int getTime(){
		return time;
	}
	
	public int getSize(){
		return width;
	}
	
	public void setTime(int newTime){
		time = newTime;
	}
	
	/** IDrawable *************************************************************/
	public void draw(Graphics2D g2d){
		AffineTransform saveAT = g2d.getTransform();
		
		g2d.transform(myTranslation);
		
		g2d.setColor(getColor());
		g2d.fillRoundRect((int)x, (int)y, width, height, 5, 5);
		if(selected){
			g2d.setColor(GameWorld.SELECTED_COLOR);
			g2d.drawRect((int)x, (int)y, width, height);
		}
		
		myTransform = g2d.getTransform();
		g2d.setTransform(saveAT);
	}
	
	public AffineTransform getTranslation() {
		return null;
	}

	/** ICollider *************************************************************/
	public boolean collidesWith(ICollider obj) { return false; }

	public void handleCollision(ICollider obj) { 
		try {
			Thread.sleep(GameWorld.COLLIDE_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/** ISelectable ***********************************************************/
	public void setSelected(boolean yn) {
		selected = yn;
	}

	public boolean isSelected() {
		return selected;
	}

	public boolean contains(Point2D p) {
		Point2D localPoint;
		/* Change from world point p to local point */
		try {
			localPoint = myTransform.createInverse().transform(p, null);
		} catch (NoninvertibleTransformException e) {
			throw new RuntimeException(e);
		}
		
		float localPointx = (float)localPoint.getX();
		float localPointy = (float)localPoint.getY();
		
		if( (localPointx >= -width/2) && (localPointx <= width/2) &&
				(localPointy >= -height/2) && (localPointy <= height/2))
			return true;
		else
			return false;
	}
}