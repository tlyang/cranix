package tly.cranix.strategy;

public interface ISmartBombStrategy{
	public static int DEFAULT = 0;
	public static int CHASE = 1;
	
	public abstract int strategy();
}