package tly.cranix;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.Iterator;

import javax.swing.JPanel;

import tly.cranix.command.PanCommand;
import tly.cranix.command.SelectCommand;
import tly.cranix.command.ZoomCommand;
import tly.cranix.model.FieldSquare;
import tly.cranix.model.GameObject;
import tly.cranix.type.IDrawable;
import tly.cranix.type.IObserver;

public class MapView extends JPanel implements IObserver{
	private GameWorldProxy gwp;
	private GameWorld g;
	private SelectCommand selectCmd;
	private ZoomCommand zoomCmd;
	private PanCommand panCmd;
	private AffineTransform theVTM;

	public MapView(GameWorld g, GameWorldProxy gwp){
		this.gwp = gwp;
		this.g = g;
		theVTM = new AffineTransform();
		selectCmd = new SelectCommand(g, theVTM);
		zoomCmd = new ZoomCommand(g);
		panCmd = new PanCommand(g);
		
		this.addMouseListener(selectCmd);
		this.addMouseMotionListener(selectCmd);
		
		this.addMouseListener(panCmd);
		this.addMouseMotionListener(panCmd);
		
		this.addMouseWheelListener(zoomCmd);
	}
	
	public JPanel getMapPanel(){ return this; }
	
	public void update(GameWorldProxy gwp){
		this.gwp = gwp;
		this.repaint();
	}
	
	private AffineTransform buildWorldToNDXform(double width, double height, double left, double bottom){
		AffineTransform worldToND = new AffineTransform();
		worldToND.scale(1/width, 1/height);
		worldToND.translate((-1)*left, (-1)*bottom);
		return worldToND;
	}
	
	private AffineTransform buildNDToScreenXform(double panelWidth, double panelHeight){
		AffineTransform ndToScreen = new AffineTransform();
		ndToScreen.translate(0, panelHeight);
		ndToScreen.scale(panelWidth, -panelHeight);
		return ndToScreen;
	}
	
	private void print(String s){
		GameWorld.print(s);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D)g;
		Iterator<GameObject> goItr = gwp.getGameCollection().iterator();
		Iterator<GameObject> fsItr = gwp.getFieldCollection().iterator();
		AffineTransform saveAT, worldToND, ndToScreen;
		GameObject go;
		
		double width = gwp.windowWidth();
		double height = gwp.windowHeight();
				
		saveAT = g2d.getTransform();
		
		/* update the Viewing Transformation Matrix */
		worldToND = buildWorldToNDXform(width, height, gwp.getWinLeft(), gwp.getWinBot());
		
		ndToScreen = buildNDToScreenXform(this.getWidth(), this.getHeight());
		
		theVTM = (AffineTransform) ndToScreen.clone();
		
		/* concatenate the VTM onto the g2d�s current transformation */
		theVTM.concatenate(worldToND);
		
		g2d.transform (theVTM);
		
		/** Draw the game objects */
		while(fsItr.hasNext()){
			go = (GameObject)fsItr.next();
			if(go instanceof IDrawable)
				((IDrawable)go).draw(g2d);
		}
		
		while(goItr.hasNext()){
			go = (GameObject)goItr.next();
			if(go instanceof IDrawable){
				((IDrawable)go).draw(g2d);
			}
		}
		
		gwp.getCar().draw(g2d);
				
		g2d.setTransform(saveAT);
	}
}