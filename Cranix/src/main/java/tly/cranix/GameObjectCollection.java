package tly.cranix;

import java.util.ArrayList;
import java.util.Iterator;

import tly.cranix.model.GameObject;


public class GameObjectCollection implements Iterable<GameObject>{
	private ArrayList<GameObject> objectCollection;

	private class GOIterator implements Iterator<GameObject>{
		private int current;

		public GOIterator(){
			current = -1;
		}
		
		public boolean hasNext() {
			if(objectCollection.size() <= 0) return false;
			if(current == objectCollection.size()-1) return false;
			return true;
		}

		public GameObject next() {
			current++;
			return objectCollection.get(current);
		}

		public void remove() {
			objectCollection.remove(current);
			--current;
		}
	}
	
	// Constructor
	public GameObjectCollection(){
		objectCollection = new ArrayList<GameObject>();
	}
	
	public GameObjectCollection(int capacity){
		objectCollection = new ArrayList<GameObject>(capacity);
	}
	
	public GameObject at(int index){
		return objectCollection.get(index);
	}
	
	public void add(GameObject g) {
		objectCollection.add(g);
	}
	
	public void clear(){
		objectCollection.clear();
	}
	
	public boolean isEmpty() {
		return objectCollection.isEmpty();
	}

	public Iterator<GameObject> iterator() {
		return new GOIterator();
	}

	public int size() {
		return objectCollection.size();
	}
}