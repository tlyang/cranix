package tly.cranix.type;

import java.awt.Color;

public interface IChangeableColor{
	abstract void changeColor(Color clr);
}