package tly.cranix.type;

import tly.cranix.GameObjectCollection;
import tly.cranix.ObserverCollection;
import tly.cranix.model.Car;


public interface IGameWorld{
	public Car getCar();
	public GameObjectCollection getGameCollection();
	public GameObjectCollection getFieldCollection();
	public int getLevelTicketTime();
	public ObserverCollection getObserverCollection();
	public boolean getSoundState();
	public int getNonOwnedSqrs();
	public int getClock();
	public boolean getClockState();
	public int getLivesLeft();
	public double getScore();
	public int getOwnedSqrs();
	public int getLevel();
	public double getMinScore();
	public boolean reachedMinScore();
	public double getWinTop();
	public double getWinRight();
	public double getWinLeft();
	public double getWinBot();
	public double windowWidth();
	public double windowHeight();
}