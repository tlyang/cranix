package tly.cranix.type;

import java.awt.Color;

import tly.cranix.GameWorld;
import tly.cranix.model.GameObject;
import tly.cranix.model.Location;

public abstract class Moveable extends GameObject{
// Static Constant headings
	public static final int NORTH = 0;
	public static final int EAST = 90;
	public static final int SOUTH = 180;
	public static final int WEST = 270;
	
	private int speed;
	private double heading;
	
	public Moveable(Color clr, int newHeading, int newSpeed, Location location){
		super(clr, location);
		speed = newSpeed;
		heading = newHeading;
	}
	
	public abstract void move(GameWorld gw); 
	
	public void setHeading(double theta){
		if(this instanceof ISteerable)
			heading = theta;
	}
	
	public void setSpeed(int newSpeed){
		speed = newSpeed;
	}
	
	public int getSpeed(){
		return speed;
	}
	
	public double getHeading(){
		return heading;
	}
}