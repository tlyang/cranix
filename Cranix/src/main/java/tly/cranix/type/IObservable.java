package tly.cranix.type;

public interface IObservable{
	public abstract void addObserver(IObserver obs);
	public abstract void notifyObservers();
}