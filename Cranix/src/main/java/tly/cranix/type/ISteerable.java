package tly.cranix.type;

public interface ISteerable{
	abstract public void changeHeading(double dir);
}