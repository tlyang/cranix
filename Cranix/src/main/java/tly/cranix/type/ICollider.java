package tly.cranix.type;

public interface ICollider{
	public abstract boolean collidesWith(ICollider obj);
	public abstract void handleCollision(ICollider obj);
}