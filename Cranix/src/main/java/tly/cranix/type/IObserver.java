package tly.cranix.type;

import tly.cranix.GameWorldProxy;

public interface IObserver{
	public void update(GameWorldProxy gwp);
}