package tly.cranix.type;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public interface IDrawable{
	public abstract void draw(Graphics2D g2d);
	public abstract AffineTransform getTranslation();
}