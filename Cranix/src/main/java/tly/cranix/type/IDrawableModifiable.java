package tly.cranix.type;

public interface IDrawableModifiable extends IDrawable {
	public abstract void rotate(double theta);
	public abstract void translate(double x, double y);
	public abstract void scale(int amt);
}
