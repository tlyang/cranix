package tly.cranix.type;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;

public interface ISelectable {
	public abstract void setSelected(boolean yn);
	public abstract boolean isSelected();
	public abstract boolean contains(Point2D p);
	public abstract void draw(Graphics2D g2d);
}
